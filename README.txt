tt.py is a tool to generate a Verilog module that if simulated prints the truth table of the input verilog expression.
tt.sh is a script that simulates that invokes tt.py and iverilog to simulate the generated module. 
For more information visit https://fdblog.xyz/circuits/truth-table-generator/.


Examples:

source tt.sh '  ( a & c ) | ( ! b ) '

['a', 'c', 'b']
0001
1001
0101
1101
0010
1010
0110
1111 


python tt.py ' ( a & c ) | ( ! b ) '

module tt();
reg [2:0] cnt;
wire a = cnt[0];
wire c = cnt[1];
wire b = cnt[2];
wire res = ( a & c ) | ( ! b ) ;
initial begin
$display("['a', 'c', 'b']");
cnt = 0;
#1;
while ( !(& cnt) ) begin
 cnt = cnt + 1;
 $display("%b%b%b%b", a, c, b, res);
 #1;
end
cnt = cnt + 1;
$display("%b%b%b%b", a, c, b, res);
#1;
end
endmodule

