import sys
import re

# get uniq element list from a string

def uniq( ls_str ):
    ls = ls_str.split()
    u_ls = []
    for e in ls:
        if e not in u_ls:
            u_ls.append(e)
    return u_ls

# usage pyton tt.py "verilog expression"
# usage pyton tt.py "( a & b ) || ( ! c ) "

if __name__ == '__main__': 
    
    # get the input arguments
    
    inputs = sys.argv[1:]
    
    # remove special characters and get all the variables
    
    litterals = uniq(re.sub("[:!#@$%&*(){};,.<>?+=\|/\"~-]" , " ", inputs[0]));
    
    # generate a verilog module
    
    print ( "module tt();" )
    
    # generate a counter of the size of the number of variables
    
    print ( "reg ["+str(len(litterals)-1)+":0] cnt;" )
    
    # assigning each bit of the counter to a variable
    
    for i , litteral in enumerate( litterals):
        print ( "wire " + litteral + " = cnt["+ str(i)+"];" )
    
    # assigning the input expression
    
    print ( "wire res = " + inputs[0] + ";" )

    # generate all the possible permutations for the variables with printing statements
    
    print ( "initial begin" )
    print ( "$display(\"" + str( litterals ) + "\");" )
    print ( "cnt = 0;" )
    print ( "#1;" )
    print ( "while ( !(& cnt) ) begin" )
    print ( " cnt = cnt + 1;" )
    print ( " "+"$display(\"" + (len(litterals)+1)*"%b" + "\", " + ", ".join(litterals) + ", res);" )
    print ( " #1;" )
    print ( "end" )
    print ( "cnt = cnt + 1;" )
    print ( "$display(\"" + (len(litterals)+1)*"%b" + "\", " + ", ".join(litterals) + ", res);" )
    print ( "#1;" )
    print ( "end" )
    print ( "endmodule" )
